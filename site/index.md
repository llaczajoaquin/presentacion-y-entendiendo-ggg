
# Main @ GuayaHack

## Tecnología en Comunidad

Somos un grupo de estudio y [Hackerspace](https://en.wikipedia.org/wiki/Hackerspace) creado por y para la {doc}`/community/index` el cual surgió como idea en [/r/Colombia](https://www.reddit.com/r/Colombia/comments/151fkiz/con_una_prima_y_un_amigo_armaremos_un_grupo_de). `GuayaHack` nació el `16 de julio de 2023` a las `20:00:00 CEST`, marcando el inicio de ésta aventura. 

```console
$ date=`date -d @1689530400`; echo -e "\nGuayaHack, fundado: $date"

GuayaHack, fundado: So 16. Jul 20:00:00 CEST 2023
```

## Sobre GuayaHack

*GuayaHack* es un juego de palabras entre *Guayacán*, el nombre coloquial del árbol [Handroanthus chrysanthus](https://en.wikipedia.org/wiki/Handroanthus_chrysanthus), y el término [Hacker](https://es.wikipedia.org/wiki/Hacker), utilizado para referirse a entusiastas de la tecnología. Inclusive tenemos un poema inspirado en ésta curiosa combinación titulado {doc}`/community/poema-cronicas-guayahack`.

Puedes aprender más sobre GuayaHack y sus miembros en la página de la {doc}`/community/index`, leyendo nuestro {doc}`/community/memorial` o echándole un vistazo las [Reglas](community/rules.md). 


```{div} discord-widget
<iframe src="https://discord.com/widget?id=1130256195345727560&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
```

## ¿Quién puede Participar?

%TODO:write las descripciones de los niveles y añadir ésta linea
%{doc}`/wiki/organizacion-nivel-novato`, {doc}`/wiki/organizacion-nivel-experimentado` y {doc}`/wiki/organizacion-nivel-profesional`

En GuayaHack tod@s, sin importar el nivel, estudiamos, practicamos y compartimos junt@s la programación, la informática y otras tecnologías relevantes en el mundo moderno entre nosotr@s.

Para unirte solo usa [éste link de invitación](https://discord.gg/trzuezGrZd) para entrar a nuestro Discord y seguir los pasos en `#rules`.

Sin importar si eres principiante, tienes experiencia o eres profesional, comenzarás resolviendo {doc}`/tarea/on-boarding-git-gitlab` y luego decidirás que camino quieres tomar.

```{toctree}
:maxdepth: 1
:hidden:
:caption: "# GuayaHack ☝️"
# Memorial <community/memorial.md>
# Noticias <noticias.md>
# Calendario <calendario.md>
# Posts <posts/index.md>
# Reglas <community/rules.md>
# Comunidad <community/index.md>
# WIKI <https://guayahack.co/posts/category/wiki/>
# ¿Cómo Ayudo? <https://gitlab.com/guayahack/main/-/issues/>
```

## Guías, Materiales y Otros

Esa tal documentación no existe, por eso hay que crearla. Escriba primero y piense después, por ejemplo la [#WIKI](https://guayahack.co/posts/category/wiki/) 
